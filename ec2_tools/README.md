# ec2-tools project
The goal of this project is to automate the creation of AWS instances as defined in an inventory file (not ansible inventory). Each "host role" will define the number of instances, the AMI, instance type and assign a host_role tag. The host role tag will be used (in another playbook) to assign a hostname for each instance.

hostname = host_role + number. The script will create and/or start instances until a complete set of sequentially named hosts exist for each role.

Secondarily, a shutdown script should exist to either stop or terminate all hosts or hosts of a particular host_role.

## Playbooks
launch.yml - launches instances

## Other things to know

env.sh - source this file to set the AWS credential environment vars.

## References
https://docs.ansible.com/ansible/2.6/modules/ec2_module.html

https://stackoverflow.com/questions/47563848/ansible-get-tag-name-and-set-hostname-over-multiple-hosts

May or may not be useful:
https://docs.ansible.com/ansible/2.5/plugins/lookup/sequence.html
