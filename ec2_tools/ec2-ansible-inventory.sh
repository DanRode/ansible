#!/usr/bin/env bash
while read i; do
    i=`sed -e "s/^ //" <<< $i`
    sed -e "s/ / ansible_host=/g" <<< $i
done < <(aws ec2 describe-instances --query 'Reservations[].Instances[].{Name:Tags[?Key==`Name`]|[0].Value,PublicIP:PublicIpAddress}' --output text) 



aws ec2 describe-instances --query 'Reservations[].Instances[].{Name:Tags[?Key==`Name`]|[0].Value,PublicIP:PublicIpAddress}' --output text
