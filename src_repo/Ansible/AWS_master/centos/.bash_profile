# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

export PATH=$PATH:$HOME/bin

ncolors=$(tput colors)
if [ -n "$ncolors" ] && [ $ncolors -ge 8 ]; then
    red=$(tput setaf 1)
    green=$(tput setaf 2)
    yellow=$(tput setaf 3)
    blue=$(tput setaf 4)
    magenta=$(tput setaf 5)
    cyan=$(tput setaf 6)
    reset=$(tput sgr0)
    bold=$(tput bold)
    # If this is not a root user, we use a green prompt

    PS1="\[$green\]\u@\h\[$reset\] (\w)\\n\$\[$reset\] "

else
    PS1="\u@\h (\w)\\n\$ "
fi
export PS1



# Some useful aliases
alias ll='ls --color=always -l | more'
alias laa='ls --color=always -lA | more'
alias lg='ls --color=always -lA | grep'
alias dir='ls -lA | grep ^d'
alias ls='ls --color=always'
alias lsh='ssh -A root@localhost'

export PATH
get_ssh_agent()
    {
    # see if we already have an agent
    ssh-add -l
    test $? -eq 2 || return

    # lookup agent on this host
    agent_out="$HOME/.ssh/agent.out"
    . "$agent_out"
    ssh-add -l
    test $? -eq 2 || return

    # start a new agent on this host
    ssh-agent >"$agent_out"
    . "$agent_out"
    # Add key to agent
    ssh-add
    }
get_ssh_agent

export ANSIBLE_NOCOWS=1
