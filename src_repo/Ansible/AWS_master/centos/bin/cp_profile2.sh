thishost=`hostname`
for i in `cat /var/lib/cf/inputs/supert/use_centoscore`; do
    if [ $i == $thishost ]; then
        echo "Skipping source host"
    else
        scp -rpq -o ConnectTimeout=2 /home/drode/.bash_profile $i:/home/drode/
        scp -rpq -o ConnectTimeout=2 /home/drode/.vimrc $i:/home/drode/
        ssh -A $i "rm -rf /home/drode/bin"
        scp -rpq -o ConnectTimeout=2 /home/drode/bin $i:/home/drode/
    fi
done
