set nocompatible
set smartindent
set autoindent
set visualbell
set t_vb=
set splitbelow
set splitright
"set guifont=7x13
set bs=2
"set tabstop=4
set shiftwidth=4
set expandtab
map q :q
syntax on
inoremap # X#

"highlight matching parentheses.
set showmatch
" type ahead search
":set incsearch
"No highlighted search
"set hls
set nohls
set incsearch
set ignorecase
set smartcase
set ruler
filetype indent on
filetype on
filetype plugin on
" Press Space to turn off highlighting and clear any message already
" displayed.
nnoremap <silent> <Space> :nohlsearch<Bar>:echo<CR>
"autocmd BufWritePre *.py normal m`:%s/\s\+$//e ``
"autocmd BufWritePre * :%s/\s*$//
"autocmd BufWritePre *.pl :%s/\s\+$//e
"autocmd BufWritePre *.py :%s/\s\+$//e
"autocmd BufEnter *.php :%s/\s\+$//e
"autocmd BufEnter *.php :%s/[ \t\r]\+$//e
set title
set titlestring=VIM:\ %F
let &titleold = hostname()

nnoremap <F2> :set invpaste paste?<CR>
set pastetoggle=<F2>
set showmode
command! -nargs=1 Silent
    \ | execute ':silent !'.<q-args>
    \ | execute ':redraw!'
"colorscheme delek
