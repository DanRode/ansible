parted /dev/xvdg rm 1
parted /dev/xvdg mkpart primary 1049kB 100%
parted /dev/xvdg set 1 boot on

e2fsck -fp /dev/xvdg1

mount /dev/xvdg1 /mnt
