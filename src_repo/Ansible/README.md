oc-host
======

This role prepares hosts for installation of other oc-* roles


Requirements
------------
Nothing special


Role Variables
--------------
Role variables and their defaults:

| Variable              | Description | Default Value
|-----------------------|-------------|---
|oc_root                |Global root directory for all oracle commerce roles | /opt/oracle
|oc_user                |Global user for all oracle commerce roles | commerce
|oc_group               |Global group for all oracle commerce roles | commerce
|oc_pid_dir             |Directory owned by oc_user where pid files will go. | /var/run/oracle|


Dependencies
------------
None

Example Playbook
----------------

    - hosts: atg-node
      roles:
        - { role: oc-common, oc_root: /home/vagrant/mycommerceinstall, oc_user: vagrant, oc_group: vagrant }
